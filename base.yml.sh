#!/usr/bin/env bash

cat <<-EOF
stages:
  - test

create_artifact:
  stage: test
  script:
    # should  be seeing the imported artifacts from needs!
    - echo -n "$ARTIFACT_CONTENTS" > "$ARTIFACT_PATH"
  artifacts:
    paths:
      - "$ARTIFACT_PATH"
EOF
